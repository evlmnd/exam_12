const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const config = require('../config');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const Photo = require('../models/Photo');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
    Photo.find()
        .populate('author')
        .then(result => res.send(result))
        .catch(() => res.sendStatus(500));
});

router.get('/:id', (req, res) => {
    Photo.find({_id: req.params.id})
        .then(result => {
            if (result) res.send(result);
            else res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));
});

router.get('/users/:id', (req, res) => {
    Photo.find({author: req.params.id})
        .populate('author')
        .then(result => {
            if (result) res.send(result);
            else res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));
});


router.post('/', [auth, upload.single('photo')], (req, res) => {
    const data = {
        title: req.body.title,
        author: req.body.userId
    };

    if (req.file) {
        data.photo = req.file.filename;
    }
    const photo = new Photo(data);
    photo
        .save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send({error: error}));
});

router.delete('/:id', auth, (req, res) => {
    Photo.remove({_id: req.params.id})
        .then(product => res.send(product))
        .catch(error => res.status(400).send(error))
});


module.exports = router;

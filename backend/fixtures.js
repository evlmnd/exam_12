const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Photo = require('./models/Photo');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const users = await User.create(
        {username: 'u1', password: '1', displayName: 'User One'},
        {username: 'u2', password: '2', displayName: 'User Two'}
    );

    const photos = await Photo.create(
        {title: 'having fun', photo: '1.jpeg', author: users[0]._id},
        {title: 'no fun', photo: '2.jpeg', author: users[1]._id},
        {title: '?', photo: '3.jpg', author: users[0]._id},
        {title: 'nooooo', photo: '4.jpg', author: users[1]._id},
    );


    await connection.close();
};

run().catch(error => {
    console.error('Something went wrong', error);
});

import React, {Component, Fragment} from 'react';
import CardItem from "../../components/CardItem/CardItem";
import {connect} from "react-redux";
import {CardDeck, Col} from "reactstrap";

import config from "../../config";
import {getPhotoItem, getPhotosList, getUserPhotos, removePhoto} from "../../store/actions/photosActions";
import PhotoItem from "../../components/PhotoItem/PhotoItem";

class ArtistsList extends Component {

    onGetPhotoItem = (id) => {
        this.props.getPhotoItem(id);
    };

    componentDidMount() {
        if (this.props.location.pathname === `/users/${this.props.match.params.id}`) {
            this.props.getUserPhotos(this.props.match.params.id);
        } else {
            this.props.getPhotosList();
        }
    };

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.id !== this.props.match.params.id) {

            if (this.props.location.pathname === '/') {
                this.props.getPhotosList();
            } else {
                this.props.getUserPhotos(this.props.match.params.id);
            }
        }
    };

    render() {

        let photos;
        (this.props.photos) ? (
            photos = this.props.photos.map(item => {
                let remove = (this.props.user && item.author._id === this.props.user._id);
                return <Col sm={3} key={item._id}>
                    <CardItem
                        title={item.title}
                        imageSrc={config.apiUrl + '/uploads/' + item.photo}
                        author={'By ' + item.author.displayName}
                        click={() => this.onGetPhotoItem(item._id)}
                        to={'/users/' + item.author._id}
                        remove={remove}
                        removeClick={() => this.props.removePhoto(item._id)}
                    />
                </Col>
            })
        ) : photos = null;


        let title = 'All photos';
        if (!this.props.photos || this.props.photos.length === 0) {
            title = 'Nothing here yet';
        } else if (this.props.photos && this.props.match.params.id) {
            if (this.props.photos.length > 0) {
                title = 'Photos by ' + this.props.photos[0].author.displayName;
            }
        }


        let modal;
        if (this.props.photoItem) {
            modal = <PhotoItem
                isOpen={this.props.showModal}
                onClick={this.props.closeModal}
                title={this.props.photoItem[0].title}
                imageSrc={config.apiUrl + '/uploads/' + this.props.photoItem[0].photo}
            />
        } else {
            modal = null
        }

        return (
            <Fragment>
                <h1>{title}</h1>
                <CardDeck>
                    {photos}
                </CardDeck>
                {modal}
            </Fragment>

        );
    }
}


const mapStateToProps = state => {
    return {
        photos: state.photos.photos,
        user: state.users.user,
        photoItem: state.photos.photoItem,
        showModal: state.photos.showModal
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getPhotosList: () => dispatch(getPhotosList()),
        getPhotoItem: id => dispatch(getPhotoItem(id)),
        getUserPhotos: id => dispatch(getUserPhotos(id)),
        closeModal: () => dispatch({type: 'CLOSE_MODAL'}),
        removePhoto: id => dispatch(removePhoto(id))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ArtistsList);
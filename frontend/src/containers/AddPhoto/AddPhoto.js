import React, {Component} from 'react';
import { connect } from "react-redux";
import {Button, Col, Form, FormGroup} from "reactstrap";

import FormElement from "../../components/UI/Form/FormElement";
import {addPhoto} from "../../store/actions/photosActions";

class AddPhoto extends Component {
    state = {
        title: '',
        photo: '',
        userId: this.props.user._id
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>

                <FormElement
                    propertyName="title"
                    title="Title"
                    type="text"
                    value={this.state.title}
                    onChange={this.inputChangeHandler}
                    required
                />


                <FormElement
                    propertyName="photo"
                    title="Photo"
                    type="file"
                    onChange={this.fileChangeHandler}
                    required
                />

                <FormGroup>
                    <Col sm={10}>
                        <Button type="submit">Add</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onSubmit: data => dispatch(addPhoto(data))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddPhoto);

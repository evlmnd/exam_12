import React from 'react';
import {
    Nav,
    Navbar,
    NavItem,
    NavLink
} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';

import AnonymousMenu from "./Menus/AnonymousMenu";
import UserMenu from "./Menus/UserMenu";

const Toolbar = ({user, logout}) => {
    return (
        <Navbar color="light" light expand="md">

            <Nav className="ml-auto" navbar>
                <NavItem>
                    <NavLink tag={RouterNavLink} to="/" exact>Photo Gallery</NavLink>
                </NavItem>
                {user ?
                    <NavItem><NavLink tag={RouterNavLink} to="/add_photo" exact>Add photo</NavLink></NavItem>
                    : null}

                {user ? <UserMenu user={user} logout={logout}/> : <AnonymousMenu/>}
            </Nav>
        </Navbar>
    );
};


export default Toolbar;

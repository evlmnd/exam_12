import React from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, NavLink, UncontrolledDropdown} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

const UserMenu = ({user, logout}) => (
    <UncontrolledDropdown nav inNavbar>
        <DropdownToggle nav caret>
            Hello, {user.displayName}
        </DropdownToggle>

        <DropdownMenu right>
            <DropdownItem>
                <NavLink tag={RouterNavLink} to={'/users/' + user._id} exact>Show profile</NavLink>
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem onClick={logout}>
                Logout
            </DropdownItem>
        </DropdownMenu>
    </UncontrolledDropdown>
);

export default UserMenu;
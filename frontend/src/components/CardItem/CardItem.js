import React, {Fragment} from 'react';
import {Button, Card, CardBody, CardImg, CardSubtitle, CardTitle, NavLink} from "reactstrap";
import PropTypes from 'prop-types';
import {NavLink as RouterNavLink} from "react-router-dom";


const CardItem = props => {
    return (
        <Fragment>
            <Card onClick={props.click} style={{cursor: 'pointer'}}>
                <CardImg top width="100%" src={props.imageSrc} alt={props.imageAlt}/>
                <CardBody>
                    <CardTitle>{props.title}</CardTitle>
                    <CardSubtitle>{props.subtitle}</CardSubtitle>
                </CardBody>
            </Card>
            {props.remove ? <Button color='danger' style={{'marginLeft': '15px'}} onClick={props.removeClick}>
                Remove
            </Button> : null}

            <NavLink tag={RouterNavLink} to={props.to} exact>{props.author}</NavLink>
        </Fragment>
    );
};

CardItem.propTypes = {
    imageSrc: PropTypes.string,
    imageAlt: PropTypes.string,
    title: PropTypes.string,
    author: PropTypes.string,
    subtitle: PropTypes.string,
    click: PropTypes.func,
    to: PropTypes.string,
    remove: PropTypes.bool
};

export default CardItem;
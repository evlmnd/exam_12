import React from 'react';
import {Button, CardImg, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

const PhotoItem = (props) => {
    return (
        <Modal isOpen={props.isOpen}>
            <ModalHeader>{props.title}</ModalHeader>
            <ModalBody>
                <CardImg src={props.imageSrc} alt={props.title} />
            </ModalBody>
            <ModalFooter>
                <Button color="secondary" onClick={props.onClick}>CLose</Button>
            </ModalFooter>
        </Modal>
    );
};

export default PhotoItem;
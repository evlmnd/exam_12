import axios from '../../axios-api';
import {push} from 'connected-react-router';

export const GET_PHOTOS_LIST_SUCCESS = 'GET_PHOTOS_LIST_SUCCESS';
export const GET_PHOTO_ITEM_SUCCESS = 'GET_PHOTO_ITEM_SUCCESS';
export const GET_USER_PHOTOS_SUCCESS = 'GET_USER_PHOTOS_SUCCESS';


export const getPhotosListSuccess = data => ({type: GET_PHOTOS_LIST_SUCCESS, data});
export const getPhotoItemSuccess = data => ({type: GET_PHOTO_ITEM_SUCCESS, data});
export const getUserPhotosSuccess = data => ({type: GET_USER_PHOTOS_SUCCESS, data});

export const getPhotosList = () => {
    return dispatch => {
        return axios.get('/photos').then(response => {
            dispatch(getPhotosListSuccess(response.data))
            }
        )
    };
};

export const getPhotoItem = (id) => {
    return dispatch => {
        return axios.get('/photos/' + id).then(response => {
            dispatch(getPhotoItemSuccess(response.data))
            }
        )
    };
};

export const getUserPhotos = (id) => {
    return dispatch => {
        return axios.get('/photos/users/' + id).then(response => {
            dispatch(getUserPhotosSuccess(response.data))
            }
        )
    };
};

export const addPhoto = (data) => {
    return (dispatch, getState) => {
        const headers = {
            Token: getState().users.user.token
        };
        return axios.post('/photos', data, {headers}).then(response => {
            dispatch(push('/'));
            }
        )
    };
};

export const removePhoto = (id) => {
    return (dispatch, getState) => {
        const headers = {
            Token: getState().users.user.token
        };
        return axios.delete('/photos/' + id, {headers}).then(response => {
            dispatch(push('/'));
            }
        )
    };
};
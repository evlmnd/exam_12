import {GET_PHOTO_ITEM_SUCCESS, GET_PHOTOS_LIST_SUCCESS, GET_USER_PHOTOS_SUCCESS} from "../actions/photosActions";

const initialState = {
    photos: null,
    photoItem: null,
    showModal: false
};

const photosReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_PHOTOS_LIST_SUCCESS:
            return {...state, photos: action.data};
        case GET_PHOTO_ITEM_SUCCESS:
            return {...state, photoItem: action.data, showModal: true};
        case 'CLOSE_MODAL':
            return {...state, showModal: false, photoItem: null};
        case GET_USER_PHOTOS_SUCCESS:
            return {...state, photos: action.data};
        default:
            return state;
    }
};

export default photosReducer;
